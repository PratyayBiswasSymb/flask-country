from os import name
from flask import render_template
import requests
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from datetime import datetime, timedelta
import math
import csv
from io import StringIO
from werkzeug.wrappers import Response


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///country.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Country(db.Model):
    sno = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    currency = db.Column(db.String(100), nullable=False)
    flag = db.Column(db.String(200), nullable=False)
    timezones = db.Column(db.String(10), nullable=False)

    nativeName = db.Column(db.String(300), nullable=False)
    capital = db.Column(db.String(200), nullable=False)
    population = db.Column(db.String(12), nullable=False)
    region = db.Column(db.String(200), nullable=False)
    subregion = db.Column(db.String(200), nullable=False)
    callcode = db.Column(db.String(4), nullable=False)
    area = db.Column(db.String(15), nullable=False)
    languages = db.Column(db.String(200), nullable=False)
    lat = db.Column(db.String(10), nullable=False)
    lng = db.Column(db.String(10), nullable=False)
    code = db.Column(db.String(5), nullable=False)
    borders = db.Column(db.String(50))

    def __repr__(self) -> str:
        return f"{self.sno} - {self.name} - {self.currency} - {self.flag} - {self.timezones} \n"


@app.template_filter('filter_currency')
def filter_currency(x):
    try:
        print(list(x["currencies"].values())[0]['name'])
        return list(x["currencies"].values())[0]['name']
    except:
        return 'Null'

def getCurrency(item):
    try:
        return item["currencies"][0]['name']
    except:
        return 'Null'


def getLatlng(item):
    l = []
    try:
        l.append(item["latlng"][0])
        l.append(item["latlng"][1])

    except:
        l.append('Null')
        l.append('Null')
    return l


def getTimezone(item):
    mid = int(len(item["timezones"]) / 2)
    r = item["timezones"][mid][3:]
    if len(r) == 0:
        return '+00:00'
    elif len(r) < 6:
        return (r+":00")
    else:
        return r


def getLanguages(item):
    s = item[0]["name"]
    l = len(item)

    for i in range(1, (l - 1)):
        s = s + ', ' + item[i]["name"]

    if l > 1:
        s = s + ' and ' + item[(l-1)]["name"]

    return s


def getBorders(item):
    try:
        l = item["borders"]
        s = " ".join(l)
        return s
    except:
        return ''


def checkNull(item, context):
    try:
        return item[context]
    except:
        return 'Null'


@app.template_filter('currentTime')
def currentTime(timezone):
    t = datetime.utcnow()
    sign = timezone[0]
    h = timezone[1:3]
    min = timezone[4:]

    utc = (t.hour * 3600) + (t.minute * 60)
    now = (int(h) * 3600) + (int(min) * 60)
    if sign == '-':
        now = - now

    time = utc + now

    date = datetime.utcnow()

    if time > 86400:
        date = date + timedelta(1)
        time = time % 86400
    elif time < 0:
        date = date - timedelta(1)
        time = 86400 + time

    date = date.strftime('%d %b %Y')

    ampm = 'AM' if time < 43200 else 'PM'
    currentHour = int(time / 3600)
    currentMinute = int((time % 3600) / 60)
    return str(date) + " " + str(currentHour) + ":" + str(currentMinute) + ampm


@app.template_filter('stringToList')
def stringToList(s):
    if s == 'Null':
        return ""
    return s.split()


@app.template_filter('getFlag')
def getFlag(inp):
    cell = Country.query.filter_by(code=inp).first()
    return cell.flag


@app.template_filter('getName')
def getName(inp):
    cell = Country.query.filter_by(code=inp).first()
    return cell.name


def zIndex(number):
    print(number)
    if number == 'Null':
        return '17'
    num = 17 - (math.log(float(number)) / math.log(4))
    return str(round(num, 2))


@app.template_filter('getMap')
def getMap(item):
    address = 'https://www.google.com/maps/'
    if item.lat != 'Null' and item.lng != 'Null':
        return address + "@" + item.lat + "," + item.lng + ',' + zIndex(item.area) + 'z'

    return address

@app.route('/checkdb')
def checkdb():
    allItem = Country.query.all()
    print(allItem)
    return render_template('checkdb.html')


@app.route('/')
def index():
    countryList = Country.query.all()

    return render_template('index.html', countryList=countryList)


@app.route('/details/<countryname>', methods=['GET'])
def details(countryname):
    country = Country.query.filter_by(name=countryname).first()

    return render_template('CountryDetails.html', country=country)


@app.route('/fetch')
def fetch():

    res = requests.get('https://restcountries.com/v2/all')
    array = res.json()
    for item in array:
        l = getLatlng(item)
        country = Country(name=item["name"], currency=getCurrency(item), flag=item["flags"]["svg"], timezones=getTimezone(item), nativeName=checkNull(item, "nativeName"), capital=checkNull(item, "capital"), population=checkNull(item, "population"), region=checkNull(
            item, "region"), subregion=checkNull(item, "subregion"), callcode=item["callingCodes"][0], area=checkNull(item, "area"), languages=getLanguages(item["languages"]), lat=l[0], lng=l[1], code=item["alpha3Code"], borders=getBorders(item))
        db.session.add(country)
        db.session.commit()
        # print(item["name"] + ": " + getBorders(item))

    return 'FETCHED'


@app.route('/download_countrylist_csv')
def download_countrylist_csv():
    def generate():
        data = StringIO()
        w = csv.writer(data)

        # header
        w.writerow(('Name', 'Capital', 'Timezone', 'Currency', 'Native Name', 'Population', 'Region',
                   'Sub-Region', 'Dialing code', 'Area', 'Languages', 'Latitude and Longitude', 'ISO Code'))
        yield data.getvalue()
        data.seek(0)
        data.truncate(0)

        countryList = Country.query.all()

        for item in countryList:
            latlng = str(item.lat) + ',' + str(item.lng)
            w.writerow((
                item.name,
                item.capital,
                item.timezones,
                item.currency,
                item.nativeName,
                item.population,
                item.region,
                item.subregion,
                item.callcode,
                item.area,
                item.languages,
                latlng,
                item.code
            ))
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)

    # stream the response as the data is generated
    response = Response(generate(), mimetype='text/csv')
    # add a filename
    response.headers.set("Content-Disposition","attachment", filename="CountryList.csv")
    return response
