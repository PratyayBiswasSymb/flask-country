import os
os.system('FLASK_APP=app.py FLASK_ENV=development flask run')

from app import app

if __name__ == '__main__':
    app.run(debug=True)